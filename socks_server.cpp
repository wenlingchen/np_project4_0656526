#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sstream>
#include <sys/resource.h>

#define MAXPATH 200
#define MAXLINE 1024

using namespace std;
int readline(int fd, char * ptr,int maxlen)
{
  int n,rc;
  char c;
  for(n=1;n<maxlen;n++)
  {
    if ((rc=read(fd,&c,1))==1)
    {
      *ptr++=c;
      if(c=='\n'){break;}
    }
    else if(rc==0)
    {
      if(n==1){return(0);}  //EOF, no data read
      else {break;} //EOF, some data was read
    }
    else
    {return(-1);}
  }
  *ptr=0;
  return(n);	//return length
}

int main(int argc, char *argv[])
{
  int port = atoi(argv[1]); //port number 
  int sockfd,ssock,clilen,childpid;
  struct sockaddr_in cli_addr,serv_addr;
  /////////////Open a TCP socket (ppt p.7)///////////////
  if ((sockfd=socket(AF_INET,SOCK_STREAM,0))<0)
  {cout<<"server: can't open stream socket."<<endl;}
  ////////////////////Bind (ppt p.8)////////////////////
  bzero((char *)&serv_addr,sizeof(serv_addr));
  serv_addr.sin_family=AF_INET;
  serv_addr.sin_addr.s_addr=htonl(INADDR_ANY);
  serv_addr.sin_port=htons(port);

  //port reuse
  int enable=1;
  setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int));

  if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
  {cout<<"server: can't bind local address."<<endl;}
  ////////////////////Listen (ppt p.8)////////////////////
  listen(sockfd,5); //backlog=5, the waiting amount in queue
  cout<<"Server is opened..."<<endl;

  ////////////////////Accept (ppt p.9)////////////////////
  while(1)
  {
    //accept client (ssock)
    clilen=sizeof(cli_addr);
    ssock=accept(sockfd,(struct sockaddr *)&cli_addr,(socklen_t*)&clilen);  //(socklen_t*)
    if(ssock<0) {cout<<"server: accept error..."<<endl;}
    if((childpid=fork())<0) {cout<<"server: fork error..."<<endl;}
    else if(childpid==0)  //child process of main: Accept correct
    {
      close(sockfd);  //close original socket
      unsigned char buffer[MAXLINE]={0}; //MAX input lenth:1024 char

      //Socks4_Request
      read(ssock,buffer,MAXLINE);
      unsigned char VN=buffer[0];
      unsigned char CD=buffer[1];
      unsigned int DST_PORT=buffer[2]<<8|buffer[3];  //8 bits
      unsigned int DST_IP=buffer[7]<<24|buffer[6]<<16|buffer[5]<<8|buffer[4];
      unsigned char * USER_ID=buffer+8;
      unsigned char reply=0;

      //Firewall test
      int filefd;
      char file_content[MAXLINE];
      filefd=open("socks.conf",O_RDONLY);
      char rule[10],mode[10],addr_str[40];
      unsigned char addr[4];
      unsigned char addr2[4];
      memset(addr,'\0',4);
      memset(addr2,'\0',4);
      
      cout<<"============================="<<endl;
      //printf("CD=%u\n",CD);
      while(readline(filefd,file_content,MAXLINE))
      {
        stringstream ss;
        ss<<file_content;
        ss>>rule>>mode>>addr_str;
        /////////////////////////////////demo code
        string tt=addr_str;        
        int tmp=tt.find("-");
        cout<<"tmp="<<tmp<<endl;
        if(tmp > 0)
        {
          addr[0]=(unsigned char)atoi(strtok(addr_str,"."));
          addr[1]=(unsigned char)atoi(strtok(NULL,"."));
          addr[2]=(unsigned char)atoi(strtok(NULL,"."));
          addr[3]=(unsigned char)atoi(strtok(NULL,"-"));  //-

          addr2[0]=(unsigned char)atoi(strtok(NULL,"."));
          addr2[1]=(unsigned char)atoi(strtok(NULL,"."));
          addr2[2]=(unsigned char)atoi(strtok(NULL,"."));
          addr2[3]=(unsigned char)atoi(strtok(NULL,"."));
          for(int i=0;i<4;i++)
          {
            printf("addr %u\taddr2 %u\n",addr[i],addr2[i]);
          }
        }
        else
        {
          addr[0]=(unsigned char)atoi(strtok(addr_str,"."));
          addr[1]=(unsigned char)atoi(strtok(NULL,"."));
          addr[2]=(unsigned char)atoi(strtok(NULL,"."));
          addr[3]=(unsigned char)atoi(strtok(NULL,"."));
        }
        if(strcmp(mode,"c")==0 && CD==1) //connect mode
        {
          for(int i=0;i<4;i++)
          {
            if(tmp>0) /////////have -
            {
              if((buffer[4+i]>=addr[i] && buffer[4+i]<=addr2[i]) || addr[i]==0x00) //*=0x00
              {
                reply=0x5A;//Accept
              }
              else
              {
                printf("Not same 1:a %u b %u c %u\n",addr[i],buffer[4+i],addr2[i]);
                reply=0x5B;//Reject
                break;
              }
            }
            else
            {
              if(buffer[4+i]==addr[i] || addr[i]==0x00) //*=0x00
              {
                reply=0x5A;//Accept
              }
              else
              {
                //printf("Not same 1:a%u b%u\n",addr[i],buffer[4+i]);
                reply=0x5B;//Reject
                break;
              }
            }
          }
        }
        else if(strcmp(mode,"b")==0 && CD==2)  //bind mode
        {
          for(int i=0;i<4;i++)
          {
            if(tmp>0) /////////have -
            {
              if((buffer[4+i]>=addr[i] && buffer[4+i]<=addr2[i]) || addr[i]==0x00) //*=0x00
              {
                reply=0x5A;//Accept
              }
              else
              {
                //printf("Not same 1:a%u b%u\n",addr[i],buffer[4+i]);
                reply=0x5B;//Reject
                break;
              }
            }
            else
            {
              if(buffer[4+i]==addr[i] || addr[i]==0x00) //*=0x00
              {
                reply=0x5A;//Accept
              }
              else
              {
                //printf("Not same 1:a%u b%u\n",addr[i],buffer[4+i]);
                reply=0x5B;//Reject
                break;
              }
            }
          }
        }
        if(reply==0x5A){break;}
      }
      printf("<S_IP>\t:%s\n",inet_ntoa(cli_addr.sin_addr));
      printf("<S_PORT>\t:%d\n",cli_addr.sin_port);
      printf("<D_IP>\t:%u.%u.%u.%u\n",buffer[4],buffer[5],buffer[6],buffer[7]);
      printf("<D_PORT>\t:%d\n",DST_PORT);
      if(CD==0x01) {printf("<Command>\t:CONNECT\n");}
      else {printf("<Command>\t:BIND\n");}
      if(reply==0x5A){printf("<Reply>\t:Accept\n");}
      else {printf("<Reply>\t:Reject\n");}

      //cout<<"<Content>\t:"<<reply<<endl;

      //Socks4_Reply
      if(CD==0x01) //connect mode
      {
        cout<<"~~~~CONNECT mode~~~~"<<endl;
        unsigned char package[MAXLINE];
        memset(package,'\0',MAXLINE);
        package[0]=0x00;
        //package[1]=reply;//0x5A(permit) or 0x5B(reject)
        package[1]=0x5A;
        // for(int i=2;i<8;i++)
        // {
        //   package[i]=buffer[i];
        // }
        package[2]=DST_PORT/256;
        package[3]=DST_PORT%256;
        package[4]=(DST_IP>>24)&0xFF;
        //connect mode: ip=ip in Socks_request
        //bind mode: ip=0
        package[5]=(DST_IP>>16)&0xFF;
        package[6]=(DST_IP>>8)&0xFF;
        package[7]=DST_IP&0xFF;
        
        printf("package=\n0 %u\n1 %u\n2 %u\n3 %u\n4 %u\n5 %u\n6 %u\n7 %u\n",
          package[0],package[1],package[2],package[3],package[4],package[5],package[6],package[7]);
        write(ssock,package,8);

        if(reply==0x5A)  //pass the firewall, socket connect
        {
          //set socket and connect
          cout<<"~~~~CONNECT mode:0x5A~~~~"<<endl;
          int rsock;
          struct sockaddr_in rsin;
          rsock=socket(AF_INET,SOCK_STREAM,0);
          bzero(&rsin,sizeof(rsin));
          rsin.sin_family=AF_INET;
          rsin.sin_addr.s_addr=DST_IP;
          rsin.sin_port=htons(DST_PORT);
          if(connect(rsock,(struct sockaddr *)&rsin,sizeof(rsin))==-1)
          {
            cout<<"Connect web error."<<endl;
            exit(1);
          }
          else
          {
            cout<<"Connect web success."<<endl;
          }

          //unsigned char content[MAXLINE];
          //read(rsock,content,MAXLINE);
          //printf("<Content>\t:%u%u%u%u%u\n",content[0],content[1],content[2],content[3],content[4]);

          int nfds;
          fd_set afds,rfds;
          FD_ZERO(&afds);
          FD_SET(rsock,&afds);
          FD_SET(ssock,&afds);

          nfds=getdtablesize();

          while(1)
          {
            FD_ZERO(&rfds);
            memcpy(&rfds,&afds,sizeof(rfds));
            if(select(nfds+1,&rfds,NULL,NULL,NULL)<0)
            {
              exit(1);
            }
            if(FD_ISSET(ssock,&rfds)) //read from browser
            {
              int len=0;
              char browser[MAXLINE];
              memset(browser,'\0',MAXLINE);
              len=read(ssock,browser,MAXLINE);
              if(len>0)
              {
                write(rsock,browser,len); //write to rsock
              }
              else
              {
                FD_CLR(ssock,&afds);
                close(ssock);
                close(rsock);
                break;
              }
            }
            if(FD_ISSET(rsock,&rfds)) //read from web server
            {
              int len=0;
              char server[MAXLINE];
              memset(server,'\0',MAXLINE);
              len=read(rsock,server,MAXLINE);
              if(len>0)
              {
                write(ssock,server,len);  //write to ssock
              }
              else
              {
                FD_CLR(rsock,&afds);
                close(ssock);
                close(rsock);
                break;
              }
            }
          }//end of while
        }//end of 0x5A

      }//end of connect mode
      else if(CD==0x02)  //bind mode
      {
        cout<<"~~~~BIND mode~~~~"<<endl;
        int bind_fd;
        struct sockaddr_in b_addr;
        socklen_t b_len=sizeof(b_addr);
        bind_fd=socket(AF_INET,SOCK_STREAM,0);

        bzero(&b_addr,sizeof(b_addr));
        b_addr.sin_family=AF_INET;
        b_addr.sin_addr.s_addr=htonl(INADDR_ANY);
        b_addr.sin_port=htons(INADDR_ANY);
        if(bind(bind_fd,(struct sockaddr *)&b_addr,sizeof(b_addr))<0)
        {cout<<"Bind: bind error."<<endl;}
        else{cout<<"Bind: bind success."<<endl;}
        //get socket local port number

        if(getsockname(bind_fd,(struct sockaddr *)&b_addr,&b_len)<0)
        {cout<<"Bind: getsockname error."<<endl;}
        else
        {
          cout<<"Bind: getsockname success."<<endl;
          printf("getsockname=%s(%u)\n",inet_ntoa(b_addr.sin_addr),ntohs(b_addr.sin_port));
        }
        /*
        struct sockaddr_in s_addr;
        socklen_t s_len=sizeof(s_addr);
        if(getsockname(bind_fd,(struct sockaddr *)&s_addr,&s_len)<0)
        {cout<<"Bind: getsockname error."<<endl;}
        else
        {
          cout<<"Bind: getsockname success."<<endl;
          printf("getsockname=%s(%u)\n",inet_ntoa(s_addr.sin_addr),ntohs(s_addr.sin_port));
        }
        */
        if(listen(bind_fd,5)<0)
        {
          cout<<"listen error\n";
        }
        else{cout<<"listen success.\n";}

        unsigned char package[MAXLINE];
        memset(package,'\0',MAXLINE);
        package[0]=0x00;
        //package[1]=reply;//0x5A(permit) or 0x5B(reject)
        package[1]=0x5A;
        package[2]=(unsigned char)(ntohs(b_addr.sin_port)/256);
        package[3]=(unsigned char)(ntohs(b_addr.sin_port)%256);
        for(int i=4;i<8;i++)
        {
          package[i]=0;
        }
        printf("package=\n0 %u\n1 %u\n2 %u\n3 %u\n4 %u\n5 %u\n6 %u\n7 %u\n",
          package[0],package[1],package[2],package[3],package[4],package[5],package[6],package[7]);
        write(ssock,package,8);
        cout<<"write reply\n";

        int ftp_fd;
        struct sockaddr_in ftp_addr;
        socklen_t ftplen=sizeof(ftp_addr);
        ftp_fd=accept(bind_fd,(struct sockaddr *)&ftp_addr,&ftplen);
        cout<<"ftp_fd="<<ftp_fd<<endl;
        if(ftp_fd<0){cout<<"ftp: accept error."<<endl;}
        else
        {
          cout<<"ftp: accept success."<<endl;
          if(reply==0x5A)  //pass the firewall
          {
            cout<<"~~~~BIND mode:0x5A~~~~"<<endl;
            write(ssock,package,8); //again

            //data transmission
            int nfds;
            fd_set afds,rfds;
            FD_ZERO(&afds);
            FD_SET(ftp_fd,&afds);
            FD_SET(ssock,&afds);

            nfds=getdtablesize();

            while(1)
            {
              FD_ZERO(&rfds);
              memcpy(&rfds,&afds,sizeof(rfds));
              if(select(nfds+1,&rfds,NULL,NULL,NULL)<0)
              {
                exit(1);
              }
              if(FD_ISSET(ssock,&rfds)) //read from browser
              {
                int len=0;
                char browser[MAXLINE];
                memset(browser,'\0',MAXLINE);
                len=read(ssock,browser,MAXLINE);
                if(len>0)
                {
                  write(ftp_fd,browser,len); //write to rsock
                }
                else
                {
                  FD_CLR(ssock,&afds);
                  close(ssock);
                  close(ftp_fd);
                  break;
                }
              }
              if(FD_ISSET(ftp_fd,&rfds)) //read from web server
              {
                int len=0;
                char server[MAXLINE];
                memset(server,'\0',MAXLINE);
                len=read(ftp_fd,server,MAXLINE);
                if(len>0)
                {
                  write(ssock,server,len);  //write to ssock
                }
                else
                {
                  FD_CLR(ftp_fd,&afds);
                  close(ssock);
                  close(ftp_fd);
                  break;
                }
              }
            }//end of while
          }//end of 0x5A
        }
        

      }

      //close(ssock);
      exit(0);

    } //end of child
    else  //parent process of main
    {
      close(ssock);
    }

    //wait(NULL); //kill zombie----change:SIGNAL
  } //end of while(1)
  return 0;
}